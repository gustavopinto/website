---
layout: post
author: Filipe Saraiva
category: globe
title: Lançamento do site do CCSL-UFPA
---

O site do __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) está no ar!

O CCSL-UFPA é um projeto desenvolvido na Faculdade de Computação do Instituto de Ciências Exatas e Naturais e tem por objetivo desenvolver estudos e projetos na área de software livre, tanto a nível de pesquisa (pesquisar dinâmicas e organizações de comunidades e seus membros), extensão (cursos, contribuições para projetos de software livre e desenvolvimento de novos softwares de maneira aberta) e ensino (ministraremos uma disciplina de software livre nesse período).

Em breve divulgaremos por esse veículo os cursos, chamadas para bolsas, projetos, artigos publicados, e demais resultados de nossos trabalhos.
