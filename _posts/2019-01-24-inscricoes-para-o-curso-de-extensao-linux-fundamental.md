---
layout: post
author: Filipe Saraiva
category: pencil-square
title: Inscrições para o curso de extensão "Linux Fundamental"
---

O __Centro de Competência em Software Livre da UFPA__ (CCSL-UFPA) anuncia seu primeiro curso de extensão -- "Linux Fundamental".

Nesse curso serão estudados os fundamentos do sistema operacional Linux, como os conceitos sobre software livre, o que são distribuições Linux, os principais ambientes gráficos, as principais aplicações, gerenciamento de pacotes, grupos e permissões, como instalar uma distribuição, além do básico sobre comandos shell no terminal.

A ementa completa pode ser conferida no [repositório do projeto "Cursos Livres"](https://gitlab.com/ccsl-ufpa/cursos-livres/blob/master/curso-linux.md).

Estão disponibilizadas 25 vagas e as aulas acontecerão no Labcomp-02 do ICEN/UFPA, de 28 à 31 de janeiro das 10:00 às 12:00. Aqueles que comparecerem a todas as aulas terão direito a um certificado de 8 horas.

__Atenção:__

`As inscrições são gratuitas mas aqueles que se inscreverem e, ou não comparecem ou faltarem a pelo menos uma das aulas, ficarão impossibilitados de se inscrever em cursos futuros.`

Vamos todos colaborar para que esse projeto tenha sucesso e aconteça por várias edições.

__Serviço:__
* Atividade: Curso "Linux - Fundamental
* Data: de 28 à 31 de janeiro de 2019
* Horário: das 10:00 às 12:00.
* Local: Labcomp-02 do ICEN-UFPA (Rua Augusto Corrêa n. 01 - Guamá)

__Formulário de Inscrições:__

<div class="embed-responsive embed-responsive-16by9">
<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSfWjwREXoXL_5baUs7osc3Ha3ijAkvfu4YhRXpoQJGYyW68dA/viewform?embedded=true" width="640" height="1889" frameborder="0" marginheight="0" marginwidth="0">Carregando…</iframe>
</div>
